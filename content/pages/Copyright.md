+++
title = "Copyright"
date = 2018-03-07
slug = "copyright"
weight = 1

[extra]
visible = true
color = "is-dark"
hide_date = true
+++

Copyright info for this site:

* ["the magic carp"](https://mynameismad.deviantart.com/art/the-magic-carp-390814591)
  site logo by [MyNameIsMad](https://mynameismad.deviantart.com/)
* The Name "Karpador" by The Pokémon Company
