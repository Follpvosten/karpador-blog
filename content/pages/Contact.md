+++
title = "Contact"
date = 2018-02-23

weight = 3

[extra]
visible = true
color = "is-dark"
hide_date = true
+++

Here is my contact info; if you find any kind of error, be it a typo, a grammar
error or a contentual error, please let me know. If you have any kind of ideas
or suggestions for content, you are also free to tell me.

Email: [wolfi@karpador.xyz](mailto:wolfi@karpador.xyz)

GitLab: [https://gitlab.com/Follpvosten](https://gitlab.com/Follpvosten)

GitHub: [https://github.com/Follpvosten](https://github.com/Follpvosten)

Mastodon: [@Follpvosten@bsd.network](https://bsd.network/@Follpvosten)
