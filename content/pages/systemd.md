+++
title = "My stance on systemd"
date = 2018-03-07
slug = "systemd"

weight = 0

[extra]
visible = false
hide_date = true
+++

A while ago, when someone asked me why I don't like systemd, I wrote an
approximately 10 pages and 3000 character long Telegram message (seriously,
it was really long, not even the ~xxxhdpi screen of someone in the group
could fit the whole thing on one screen).  
This document is meant as a replacement for that message that's better
formatted and better maintained.

This document was initially written on wednesday, the 7th of march 2018.

> Note: As of the 20th of october 2019, I'm way more relaxed when it comes to
> this topic; I've generally become very pragmatic and there are times when I
> just like things to work, so I occasionally run Ubuntu or Debian or even
> Fedora when I want everything to "just work".
>
> I have no idea what the content of the rest of the page will be, as it's been
> a very long time since I wrote it, and I should probably update it, but I
> don't have the time for that right now. I can only say that as of now, I
> don't care as much about my init system.
>
> Have fun anyways.

Let's start with...

## My history with systemd
Some years ago, I was using Ubuntu 14.04, exploring the wonders of Linux,
while still occasionally using Windows (a story for another time). I had not
yet realized the freedoms I had, and discovering them went somewhat hand-in-hand
with discovering systemd.

Over time, I tried out various distributions of Linux, mostly in VMs, as I
wasn't as experienced with installing it on real hardware at the time. When
I installed Arch, I first heard about that `systemctl` thing, and didn't
really think about it, I just used it and assumed it was a part of Arch.

When trying out ArchBang, I realized that there was no `systemctl`, and it
threw me off, because I was expecting it. I tried to find out why it was like
this, and I discovered that that "systemctl" program was a part of something
called `systemd`. I still didn't think much about it, I even thought it was
the better option at the time, as it booted faster.

At some point, I broke my Ubuntu 14.04 installation, and naturally, having
always relied on Ubuntu as my main Linux distro, I installed Ubuntu 16.04,
and that was the moment systemd started bothering me.

I noticed that I had some insane shutdown times of up to 5 minutes at times,
and I just couldn't find a way to fix that. I knew it had something to do with
systemd. (Boot times also sometimes did go up to 2 minutes).

At that point, I started to look more into it, to find out what systemd actually
was and why it was everywhere at that point. The more I read about it, the more
something in my head screamed "bad idea!". I decided to avoid systemd - only to
find out that it was *almost impossible*. Somehow, **all** of the major distros
had adopted systemd, and I just couldn't understand why.

## Why I dislike it
The moment I saw that systemd was already *everywhere* was the moment I decided
to do everything I can to avoid it. That alone is the main reason I don't like
it - that it becomes harder and harder to avoid it.

When I found out about systemd, it was around the time where I realized what
I liked about Unix-like systems (and especially Linux systems) so much: It was
the freedom of choice. I was the one to decide which components my system has
and uses, and I could switch them out at will. That freedom is what was driving
me towards Free Software, the freedom to use what I want and not use what I do
not want.

And systemd was taking that away, just by being everywhere. It forced its
philosophy, its software stack and its problems onto me, without a way to get
rid of them. It just felt terrible, and that's how it feels whenever I use a
systemd-based distribution to date.

That's how I started my not-yet-ended search for a new operating system that
is both free as in freedom, and systemd-free.

### Summary
The primary reason I don't like it is based on my feelings about freedom. I use
Linux and other free operating systems because they give me the power to use
whatever I want, and systemd is trying to take that away - that's how I feel,
at least. You can try changing my mind, but I don't think it's gonna happen.

## Other reasons to dislike it
If you just wanted to know my main reason to dislike it, you can stop now.

However, my personal feelings aren't the only reason to dislike systemd,
they are just my most prominent one. There are technical and philosophical
reasons as well, and of course, the attitudes of its main developers. I will
talk about some examples briefly, and then link some sources where you can
read further.

### Technical reasons
* It is a bad idea to make a giant software suite dependent on a specific PID 1.
* It is a bad idea to have a complex program as PID 1. Why does PID 1 have a dbus
  interface?
* Binary logs are terrible for debugging a non-bootable system.

(I don't have that many technical reasons because I don't care about them
that much, but have some links to read further)

* [About journald](http://blog.gerhards.net/2011/11/what-i-dont-like-about-journald.html)
* [Broken by design: systemd](http://ewontfix.com/14/)
* [efivars are mounted read/write](https://github.com/systemd/systemd/issues/2402)

### Philosophical reasons
* It breaks the Unix philosophy.

You might have heard this one before, but what does it actually mean?

The old Unix philosophy says the following for programs:
"Do one thing and do it well."

And that was always the greatest quality of Unix-like systems: Multiple
programs working together, exchanging data in simple formats (the definition
really says that all data should be a simple string), to create a really
powerful way of working with them. Unix-style pipes are probably the best
example of that. And there are init systems which really do one thing and
do it well; for example, [sinit](https://core.suckless.org/sinit). Systemd
is completely against that philosopy, by having badly documented APIs and
ramming a lot of actual functionality into the PID 1, which should ideally be
the simplest part of the system, because if PID 1 crashes, the system
crashes.

(By the way, I don't think the UNIX philosophy applies absolutely everywhere -
for example, I use KDE, which is probably one of the biggest kind-of-monolithic
projects out there, with components like the KDE PIM suite, which saves your
personal data in local MySQL databases, and so on. I only think that *critical
system components* should always follow the UNIX philosophy, because being
simple is a good way of reducing the potential for errors.)

* [More about the UNIX philosophy and systemd](http://pappp.net/?p=969)

### The developers
This will just be a list of links.

* [Lennart about su](https://github.com/systemd/systemd/issues/825#issuecomment-127917622)
* [Usernames starting with numbers are "not valid"](https://github.com/systemd/systemd/issues/6237)
* [Lennart about Linus and Linux](http://www.zdnet.com/article/lennart-poetterings-linus-torvalds-rant/)
* [Ts’o and Linus And The Impotent Rage Against systemd](https://igurublog.wordpress.com/2014/04/03/tso-and-linus-and-the-impotent-rage-against-systemd/)

There are a lot of other examples, I'm just listing the first ones I found.

### Other people's perspectives on everything
* [systemd: Please, No, Not Like This](http://fromthecodefront.blogspot.co.at/2017/10/systemd-no.html)
  (highly recommeded read)
* [Combatting revisionist history](http://forums.debian.net/viewtopic.php?f=20&t=120652&p=570371)
  (A Debian perspective)
* [Why I dislike systemd](http://www.steven-mcdonald.id.au/articles/systemd.shtml)
  (also a big recommendation)

And last, but not least, here are some curated lists of other things that happened
or are still happening with systemd:

* [Arguments against systemd](http://without-systemd.org/wiki/index.php/Arguments_against_systemd)
* [suckless.org on systemd](https://suckless.org/sucks/systemd)
  (there are some strong words in this one, but the linked stuff is worth a
   read. They are mostly about the developers' behaviour.)

## The good part
After reading about what I don't like about systemd, how about reading something
more positive?

What I like about systemd:

* It led me to try FreeBSD, which is the greatest server OS I know to this date.
* It led me to explore alternative init systems, to learn about the UNIX
  philosophy and to try things I would never have tried without systemd as the
  motivation, such as installing Gentoo.
* It serves as a great negative example of software design, showing how to
  overcomplicate development to the max.
* It actually also "explained" to me why SysVinit is bad. On my list of worst
  init systems, systemd takes the first spot, with only SysVinit coming close
  to it (there are init systems I have not actually used, so there might be
  other really bad examples, but I don't know enough about them)

