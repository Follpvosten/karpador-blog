+++
title = "About me"
date = 2018-02-23

weight = 2

[extra]
visible = true
color = "is-dark"
hide_date = true
+++

I am a Linux and BSD enthusiast, Free Software advocate and software developer.
Professionally developing software in .NET and JavaScript and hating it with a
passion, but I need money, so I don't really have a choice.

I like:

* Software Freedom
* Java
* Rust
* FreeBSD
* Linux (the kernel. I don't care about GNU that much.)
* KDE Plasma 5 (yes, specifically 5)
* runit
* Sushi (especially when it's running)

I don't like:

* Microsoft
* JavaScript
* [systemd](@/pages/systemd.md)
* Chocolate flavoured icecream (seriously. It just doesn't taste like chocolate.)
